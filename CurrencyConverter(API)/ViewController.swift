//
//  ViewController.swift
//  CurrencyConverter(API)
//
//  Created by Mustafa GUNES on 18.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//


// 18 Kasım 2017 fixer.io sitesinde json dosyasına ulaşılamıyordu. Bu yüzden proje çalışmadı. Kodlar aşağıdaki gibidir.


import UIKit

class ViewController: UIViewController, UISearchBarDelegate { // UISearchBarDelegate Eklendi.

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var usdLabel: UILabel!
    @IBOutlet weak var cadLabel: UILabel!
    @IBOutlet weak var chfLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        getCurrency(currency: (searchBar.text?.lowercased())!) // uppercased() => yazılan yazıyı büyük olarak al.
        searchBar.text = ""
    }
    
    func getCurrency(currency: String) { // döviz getir.
        
        //let url = URL(string: "https://www.doviz.com/api/v1/currencies/\(currency)/latest")
        //let url = URL(string: "http://www.floatrates.com/daily/\(currency).json")
        
        let url = URL(string: "https://api.fixer.io/latest?base=USD")
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            if error != nil { // hata varsa alert oluştur.
                
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                if data != nil {
                    
                    do {
                        
                        let jSONResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async { // büyük boyutlu bir json indirirken uygulama kilitlenmesin diye oluşturulan sistem
                            
                            print(jSONResult)
                            
                            let rates = jSONResult["rates"] as! [String : AnyObject]
                            
                            let usd = String(describing: rates["USD"]!)
                            self.usdLabel.text = "USD: \(usd)"
                            
                            let cad = String(describing: rates["CAD"]!)
                            self.cadLabel.text = "CAD: \(cad)"
                            
                            let chf = String(describing: rates["CHF"]!)
                            self.chfLabel.text = "CHF: \(chf)"
                        }
                    }
                    catch
                    {
                        let alertView = UIAlertController(title: "Error Message!", message: "Unexpected error !", preferredStyle: .alert)
                        let okButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                            (uyari: UIAlertAction) -> Void in
                        })
                        
                        alertView.addAction(okButton)
                        self.present(alertView, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
}

